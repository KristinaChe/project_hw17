
#include <iostream>
#include <cmath>

class Tree
{
public:
    Tree() : height(0), diameter(0), age(0)
    {}
    Tree(double _height, double _diameter, double _age) : height(_height), diameter(_diameter), age(_age)
    {}

    void getParameters()
    {
        std::cout << height << ' ' << diameter << ' ' << age << '\n';
    }
private:
    double height;
    double diameter;
    double age;
};

class Vector
{
public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z << '\n';
    }

    void getLength()
    {
        double m = sqrt((x * x) + (y * y) + (z * z));
        std::cout << m ;
    }
private:
    double x = 0;
    double y = 0;
    double z = 0;
};

int main()
{
    Tree t(9.5, 0.3, 15);
    t.getParameters();

    Vector v(15, 1, 3);
    v.Show();
    v.getLength();
}